import org.junit.Assert;

class Test {

    public static void main(String[] args) {

        tests(10, 10);
        tests(25, 44);
    }

    public static int fibGen(int max) {
        int a = 1;
        int b = 1;
        int c = 0;
        int result = 0;

        System.out.println("\nFibonacci sequence...\n" + a);
        System.out.println("\nFibonacci sequence...\n" + b);

        for (int i = 0; c < max; i++) {
            c = a + b;
            if (!(c > 4000000)) {
                System.out.println("Fibonacci sequence...");
                System.out.println(c + "\n");
                a = b;
                b = c;
            }
            result += sumEvenFibNums(c, 0);
        }
        return result;
    }

    public static int sumEvenFibNums(int sum, int even) {
        if (sum %2 == 0) {
            even = sum+even;
        }
        return even;
    }

    public static void tests(int max, int assertVal) {
        Assert.assertEquals(fibGen(max), assertVal);
        System.out.println(assertVal + " is equal to fibGen(max) value");
    }
}